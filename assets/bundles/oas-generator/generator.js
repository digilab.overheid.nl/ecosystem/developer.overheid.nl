import JSONEditor from "jsoneditor";
import YAML from 'yaml'
import populateOpenApiSpec from "./populateOAS";
import 'ace-one-themes';
import fileDownload from 'js-file-download';

let outputEditor = null;

initJsonEditors = function () {

    // set json
    const initialJson = {
        "title": "DON API v1",
        "description": "API of developer.overheid.nl (Developer Overheid).",
        "contact": {
            "name": "Team Developer Overheid",
            "email": "developer.overheid@geonovum.nl",
            "url": "https://developer.overheid.nl"
        },
        // Defineer hier de endpoints van je API
        "resources": [
            {
                "name": "repository",
                "plural": "repositories",
                "readonly": false
            },
            {
                "name": "api",
                "plural": "apis",
                "readonly": true
            }
        ]
    }

    const inputEditorOtions = {
        onChange: async function () {
            var errors = await inputEditor.validate();
            if (errors.length === 0) {
                const updatedJson = inputEditor.get();
                const populatedOpenApiSpec = populateOpenApiSpec(updatedJson)
                outputEditor.set(populatedOpenApiSpec)
            }
        }
    }

    const inputEditor = createEditor(inputEditorOtions, "oas-generator-input-field");
    inputEditor.set(initialJson)

    const outputEditorOptions = {};
    outputEditor = createEditor(outputEditorOptions, "oas-generator-output-field");
    outputEditor.aceEditor.setReadOnly(true);

    const populatedOpenApiSpec = populateOpenApiSpec(initialJson);
    outputEditor.set(populatedOpenApiSpec);

}

const createEditor = function (options, containerElementId) {
    options.theme = 'ace/theme/one-dark';
    const editor = new JSONEditor(document.getElementById(containerElementId), options);
    editor.setMode('code')
    return editor
}

const initYmlDownloadButton = function (elementId) {
    const buttonElement = document.getElementById(elementId);
    buttonElement.onclick = async function () {
        const updatedJson = outputEditor.get();
        console.log('fight');
        const data = await YAML.stringify(updatedJson);
        fileDownload(data, 'openapi.yaml');
    };
}

const initJSONDownloadButton = function (elementId) {
    const buttonElement = document.getElementById(elementId);
    buttonElement.onclick = async function () {
        const updatedJson = outputEditor.get();
        fileDownload(JSON.stringify(updatedJson, null, 2), 'openapi.json');
    };
}

const initCopyYmlButton = function (elementId) {
    const buttonElement = document.getElementById(elementId);
    buttonElement.onclick = async function () {
        const updatedJson = outputEditor.get();
        const data = await YAML.stringify(updatedJson);
        navigator.clipboard.writeText(data);
    };
}


initJsonEditors();
initYmlDownloadButton('yml-download-button');
initJSONDownloadButton('JSON-download-button');
initCopyYmlButton('yml-copy-button');
