import { kebabCase, upperCamelCase } from 'case-anything'

populateOpenApiSpec = function (inputJson) {
  return {
    "openapi": "3.0.0",
    "info": {
      "title": inputJson.title,
      "description": inputJson.description,
      "version": "1.0.0",
      "contact": {
        "name": inputJson.contact.name,
        "email": inputJson.contact.email,
        "url": inputJson.contact.url
      }
    },
    "servers": [
      {
        "url": "@TODO: Add server URL",
      }
    ],
    "tags": inputJson.resources.map(resource => {
      var tag = resource['plural']
      tag = toUppercase(tag)
      return { "name": tag, "description": `Alle API operaties die horen bij ${resource['plural']}.` }
    }),
    "paths": createPaths(inputJson.resources),
    "components": {
      "schemas": createSchemas(inputJson.resources),
      "parameters": {
        "id": {
          "name": "id",
          "in": "path",
          "description": "id",
          "required": true,
          "schema": {
            "type": "string"
          }
        }
      }
    }
  }
}

const toUppercase = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

const createPaths = (resources) => {
  const initialValue = {};
  return resources.reduce((obj, item) => {

    const endpointList = createEndpointList(item)
    const endpointSingle = createEndpointSingle(item)

    const pluralKebabCase = kebabCase(item['plural']);

    return {
      ...obj,
      [`/${pluralKebabCase}`]: endpointList,
      [`/${pluralKebabCase}/{id}`]: endpointSingle,
    };
  }, initialValue);
};

const createEndpointSingle = function (item) {
  const endpointSingle = {
    "parameters": [
      {
        "$ref": "#/components/parameters/id"
      }
    ],
    "get": {
      "operationId": `retrieve${upperCamelCase(item['name'])}`,
      "description": `${toUppercase(item['name'])} ophalen`,
      "summary": `${toUppercase(item['name'])} ophalen`,
      "tags": [
        toUppercase(item['plural']),
      ],
      "responses": {
        "200": {
          "headers": {
            "API-Version": {
              "$ref": "https://developer.overheid.nl/static/adr/components.yaml#/headers/API-Version"
            }
          },
          "description": "OK",
          "content": {
            "application/json": {
              "schema": {
                "$ref": `#/components/schemas/${toUppercase(item['name'])}`
              }
            }
          }
        },
        "404": {
          "$ref": "https://developer.overheid.nl/static/adr/components.yaml#/responses/404"
        }
      }
    }
  }

  if (!item.readonly) {
    endpointSingle["put"] = {
      "operationId": `edit${upperCamelCase(item['name'])}`,
      "description": `${toUppercase(item['name'])} wijzigen`,
      "summary": `${toUppercase(item['name'])} wijzigen`,
      "tags": [
        toUppercase(item['plural']),
      ],
      "responses": {
        "200": {
          "headers": {
            "API-Version": {
              "$ref": "https://developer.overheid.nl/static/adr/components.yaml#/headers/API-Version"
            }
          },
          "description": "OK",
          "content": {
            "application/json": {
              "schema": {
                "$ref": `#/components/schemas/${toUppercase(item['name'])}`
              }
            }
          }
        },
        "400": {
          "$ref": "https://developer.overheid.nl/static/adr/components.yaml#/responses/400"
        }
      }
    }
    endpointSingle["delete"] = {
      "operationId": `remove${upperCamelCase(item['name'])}`,
      "description": `${toUppercase(item['name'])} verwijderen`,
      "summary": `${toUppercase(item['name'])} verwijderen`,
      "tags": [
        toUppercase(item['plural']),
      ],
      "responses": {
        "204": {
          "$ref": "https://developer.overheid.nl/static/adr/components.yaml#/responses/204"
        },
        "404": {
          "$ref": "https://developer.overheid.nl/static/adr/components.yaml#/responses/404"
        }
      }
    }
  }

  return endpointSingle
}

const createEndpointList = function (item) {
  const endpointList = {
    "get": {
      "operationId": `list${upperCamelCase(item['plural'])}`,
      "description": `Alle ${item['plural']} ophalen`,
      "summary": `Alle ${item['plural']} ophalen`,
      "tags": [
        toUppercase(item['plural'])
      ],
      "responses": {
        "200": {
          "headers": {
            "API-Version": {
              "$ref": "https://developer.overheid.nl/static/adr/components.yaml#/headers/API-Version"
            },
            "Link": {
              "$ref": "https://developer.overheid.nl/static/adr/components.yaml#/headers/Link"
            }
          },
          "description": "OK",
          "content": {
            "application/json": {
              "schema": {
                "$ref": `#/components/schemas/${toUppercase(item['name'])}`
              }
            }
          }
        }
      }
    },
  }

  if (!item.readonly) {
    endpointList["post"] = {
      "operationId": `create${upperCamelCase(item['plural'])}`,
      "description": `Nieuwe ${item['name']} aanmaken`,
      "summary": `Nieuwe ${item['name']} aanmaken`,
      "tags": [
        toUppercase(item['plural']),
      ],
      "responses": {
        "201": {
          "headers": {
            "API-Version": {
              "$ref": "https://developer.overheid.nl/static/adr/components.yaml#/headers/API-Version"
            }
          },
          "description": "Created",
          "content": {
            "application/json": {
              "schema": {
                "$ref": `#/components/schemas/${toUppercase(item['name'])}`
              }
            }
          }
        },
        "400": {
          "$ref": "https://developer.overheid.nl/static/adr/components.yaml#/responses/400"
        }
      }
    }
  }

  return endpointList

}

const createSchemas = (resources) => {
  const initialValue = {};
  return resources.reduce((obj, item) => {

    const objSchema = {
      properties: {
        id: {
          type: "string",
          format: "uuid",
        }
      }
    };

    return {
      ...obj,
      [`${upperCamelCase(item['name'])}`]: objSchema,
    };
  }, initialValue);
};

export default populateOpenApiSpec;
