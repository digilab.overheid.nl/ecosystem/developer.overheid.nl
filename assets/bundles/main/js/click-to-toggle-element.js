const initClickToToggle = () => {
    const clickToToggleButtons = document.getElementsByClassName("js-click-to-toggle");

    Array.from(clickToToggleButtons).forEach(
        function (clickToToggleButton) {
            const elementToToggle = document.getElementById(clickToToggleButton.getAttribute("aria-controls"));
            const targetHiddenLabel = clickToToggleButton.getAttribute("data-label-target-hidden");
            const targetShownLabel = clickToToggleButton.getAttribute("data-label-target-shown");

            clickToToggleButton.onclick = function () {
                if (window.getComputedStyle(elementToToggle).display !== "none") {
                    elementToToggle.style.display = "none";
                    clickToToggleButton.innerHTML = targetShownLabel;
                    clickToToggleButton.setAttribute("aria-expanded", "false");
                } else {
                    elementToToggle.style.display = "block";
                    clickToToggleButton.innerHTML = targetHiddenLabel;
                    clickToToggleButton.setAttribute("aria-expanded", "true");
                }
            }
        }
    );
};

document.addEventListener("htmx:afterSwap", (_) => {
    initClickToToggle();
});

initClickToToggle();
