const init = () => {
    const buttons = document.getElementsByClassName("js-mutate-classname-of-target");

    Array.from(buttons).forEach(
        function (button) {
            const elementToToggle = document.getElementById(button.getAttribute("aria-controls"));
            const cssToggleClass = button.getAttribute("data-css-classname");
            const action = button.getAttribute("data-action");

            button.onclick = function () {
                if (action === 'remove') {
                    elementToToggle.classList.remove(cssToggleClass);
                    elementToToggle.scrollIntoView();
                } else if (action === 'add') {
                    elementToToggle.classList.add(cssToggleClass);
                } else {
                    return
                }
            }
        }
    );
};

document.addEventListener("htmx:afterSwap", (_) => {
    init();
});

init();
