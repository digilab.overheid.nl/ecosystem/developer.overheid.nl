let mobileNavigationToggle = document.getElementById("mobile-navigation-toggle");
let mobileNavigationCloseOnClickElements = document.getElementsByClassName("mobile-navigation-close-on-click");
let mobileNavigationBackdrop = document.getElementById("mobile-navigation-backdrop");
let drawer = document.getElementById("mobile-navigation-drawer");

const activeClass = "active";

mobileNavigationToggle.onclick = () => {
    const drawerCollapsed = mobileNavigationToggle.getAttribute("aria-expanded");

    if (drawerCollapsed === "false") {
        drawer.classList.add(activeClass)
        mobileNavigationBackdrop.classList.add(activeClass)
        mobileNavigationToggle.setAttribute("aria-expanded", "true")
    } else {
        drawer.classList.remove(activeClass)
        mobileNavigationBackdrop.classList.remove(activeClass)
        mobileNavigationToggle.setAttribute("aria-expanded", "false")
    }
}

for (let element of mobileNavigationCloseOnClickElements) {
    element.onclick = () => {
        drawer.classList.remove(activeClass)
        mobileNavigationBackdrop.classList.remove(activeClass)
        mobileNavigationToggle.setAttribute("aria-expanded", "false")
    }
}
