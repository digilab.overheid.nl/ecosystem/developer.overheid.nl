import { BarChart, LineChart } from "chartist";
import "chartist/dist/index.scss";
import "chartist-plugin-tooltips-updated/dist/chartist-plugin-tooltip.css";

// All Chartist options can be found here: https://codepen.io/k3no/pen/jMNGBR

const dataDesignRulesSuccesses = JSON.parse(document.getElementById("graph-data-api-design-rules-per-score").textContent);
const firstDataSet = Object.values(dataDesignRulesSuccesses)[0];

const barChart = new BarChart(
    "#js-bar-chart-api-design-rules-successes",
    {
        labels: Object.keys(firstDataSet),
        series: Object.values(firstDataSet),
    },
    {
        distributeSeries: true,
        showScale: false,
    },
);

const options = {
    labelOffset: { x: -13, y: -6 },
    labelClass: "label"
};

barChart.on("draw", function (data) {
    if (data.type === "bar") {
        data.group.elem("text", {
            x: (data.x === undefined ? data.x1 : data.x) + (data.series.toString().length == 2 ? 4 : 0) + options.labelOffset.x,
            y: (data.y === undefined ? data.y2 : data.y) + options.labelOffset.y,
            style: "text-anchor: " + options.textAnchor
        }, options.labelClass).text(data.series);
    }
});


function createLinechart(id, data, tooltipClass) {

    let graphYMax;
    let graphYMin;

    const minValue = Math.min(...data["series"]);
    const maxValue = Math.max(...data["series"]);

    if (minValue > 200) {
        const diff = maxValue - minValue;
        graphYMin = minValue - (diff * 0.5);
        graphYMax = maxValue + (diff * 0.5);
    } else {
        graphYMin = minValue - 10;
        graphYMax = maxValue + 10;
    }

    const labels = data["labels"]
    const lineChart = new LineChart(id, { labels, series: [data["series"]] }, {
        fullWidth: true,
        chartPadding: {
            right: 40
        }, axisY: {
            onlyInteger: true,
        },
        low: graphYMin,
        high: graphYMax
    });

    lineChart.on("draw", function (datapoint) {
        if (datapoint.type === "point") {
            datapoint.element._node.addEventListener("mouseenter", (_) => {
                const tooltip = document.getElementsByClassName(tooltipClass);
                tooltip[0].style.top = datapoint.y - 100 + "px";
                tooltip[0].style.left = datapoint.x - 56 + "px";
                tooltip[0].classList.remove("hidden");

                document.querySelector(`.${tooltipClass} > .chartist-tooltip-meta`).innerHTML = labels[datapoint.index];
                document.querySelector(`.${tooltipClass} > .chartist-tooltip-value`).innerHTML = `Aantal: ${datapoint.value.y}`;
            });

            datapoint.element._node.addEventListener("mouseleave", (_) => {
                const tooltip = document.getElementsByClassName(tooltipClass);
                tooltip[0].classList.add("hidden");
            });
        }
    });
}

const numberOfApisJson = JSON.parse(document.getElementById("graph-data-api-totals").textContent);
createLinechart("#js-number-of-apis-chart", numberOfApisJson, "js-chartist-tooltip");

const numberOfOrgs = JSON.parse(document.getElementById("graph-data-organization-totals").textContent);
createLinechart("#js-number-of-orgs", numberOfOrgs, "js-chartist-tooltip-orgs");

const numberOfRepos = JSON.parse(document.getElementById("graph-data-repository-totals").textContent);
createLinechart("#js-number-of-repos", numberOfRepos, "js-chartist-tooltip-repos");
