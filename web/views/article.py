from django.views.generic import DetailView, ListView
from django.utils.safestring import mark_safe
from django.urls import reverse, reverse_lazy
from django.core.paginator import Page

from core.models.article import Article

from web.views.generic import BaseView
from web.views.mixins import Breadcrumb
from .generic import BaseView


class ArticleListView(ListView, BaseView):
    template_name = "article_list.html"
    model = Article
    title = "Artikelen"
    breadcrumb = Breadcrumb(title=title, url=reverse_lazy("web:article_list"))
    paginate_by = 9
    pages_on_each_side = 1
    pages_on_ends = 1
    page_kwarg = "pagina"

    def get_queryset(self):
        return Article.objects.published()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        page: Page = context["page_obj"]
        context["page_range"] = page.paginator.get_elided_page_range(
            page.number, on_each_side=self.pages_on_each_side, on_ends=self.pages_on_ends)
        context["page_kwarg"] = self.page_kwarg

        return context


class ArticleDetailView(DetailView, BaseView):
    template_name = "article_detail.html"
    model = Article
    object: Article

    def get_title(self):
        return self.object.title

    def get_description(self):
        return self.object.summary or ""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        article = self.object
        context["content_safe_html"] = mark_safe(article.content_html)
        return context

    def get_breadcrumbs(self):
        return [
            ArticleListView.breadcrumb,
            Breadcrumb(self.get_title(), reverse(
                "web:article_detail", kwargs={"slug": self.object.slug}))
        ]
