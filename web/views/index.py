from core.models.article import Article

from .generic import TemplateView


class IndexView(TemplateView):
    template_name = "index.html"
    description = "Eén centrale plek voor de developer die voor of met de overheid ontwikkelt"
    apis_limit = 10
    articles_limit = 3

    def get_breadcrumbs(self):
        return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["article_list"] = Article.objects.published()[:self.articles_limit]

        return context
