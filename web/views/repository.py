from django.db.models import F
from django.urls import reverse_lazy
from django.views.generic import FormView

from core.models.repository import Repository

from .generic import FacetListView, FacetFilter, BaseView, TemplateView
from .mixins import Breadcrumb

from ..forms import AddRepositoryForm


class RepositoryListView(FacetListView):
    template_name = "repository_list.html"
    partial_template_name = "partials/repository_list_results.html"
    title = "Repository's"
    description = "Overzicht van open source projecten van Nederlandse overheidsorganisaties."
    breadcrumb = Breadcrumb(title, reverse_lazy('web:repository_list'))

    queryset = (
        Repository.objects.prefetch_related(
            "programming_languages",
            "repositoryprogramminglanguage_set",
            "related_apis",
            "topics"
        )
        .order_by(F("last_change").desc(nulls_last=True))
    )
    text_search_label = "Vind repository's"
    text_search_fields = ("owner_name", "name", "description")
    facets_filters = {
        "organisatie": FacetFilter(
            label="Organisatie", field_name="organization", choice_label_field="organization__name"),
        "technologie": FacetFilter(
            label="Technologie", field_name="programming_languages", choice_label_field="programming_languages__name"),
        "gearchiveerd": FacetFilter(
            label="Gearchiveerde repository's",
            field_name="archived",
            choice_label_field="archived",
            choices={True: "Toon alleen gearchiveerde"},
        ),
    }


class RepositoryAddView(FormView, BaseView):
    template_name = "repository_add.html"
    title = "Repository laten toevoegen"
    form_class = AddRepositoryForm
    success_url = reverse_lazy("web:repository_add_success")
    breadcrumb = Breadcrumb("toevoegen", reverse_lazy("web:repository_add"))

    def form_valid(self, form: AddRepositoryForm):
        form.create_issue()
        return super().form_valid(form)

    def get_breadcrumbs(self):
        return [
            RepositoryListView.breadcrumb,
            self.breadcrumb
        ]


class RepositoryAddSuccessView(TemplateView):
    title = "Account geregistreerd"
    template_name = "repository_add_success.html"
    breadcrumb = Breadcrumb("succes", reverse_lazy("web:repository_add_success"))

    def get_breadcrumbs(self):
        return [
            RepositoryListView.breadcrumb,
            RepositoryAddView.breadcrumb,
            self.breadcrumb
        ]
