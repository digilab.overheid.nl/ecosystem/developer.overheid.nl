from django.conf import settings
from django.urls import path
from django.views import defaults as default_views
from django.views.generic.base import RedirectView

from .views.api import (
    APIListView, APIDetailView, APISpecificationView, APIAddView, 
    APIAddSuccessView, APIValidatorReportsView, OpenApiSpecGenerator
)
from .views.article import ArticleListView, ArticleDetailView
from .views.generic import TemplateView
from .views.index import IndexView
from .views.organization import OrganizationDetailView, OrganizationListView
from .views.repository import RepositoryListView, RepositoryAddView, RepositoryAddSuccessView
from .views.statistics import StatisticsView


app_name = "web"


urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("apis", APIListView.as_view(), name="api_list"),
    path("apis/toevoegen", APIAddView.as_view(), name="api_add"),
    path("apis/toevoegen/succes", APIAddSuccessView.as_view(), name="api_add_success"),
    path("apis/<slug:api_id>", APIDetailView.as_view(), name="api_detail"),
    path("apis/<slug:api_id>/score-details", APIValidatorReportsView.as_view(), name="api_validator_reports"),
    path("apis/<slug:api_id>/specificatie/<str:environment_name>", APISpecificationView.as_view(),
         name="api_specification"),
    path("organisaties", OrganizationListView.as_view(), name="organization_list"),
    path("organisaties/<slug:slug>", OrganizationDetailView.as_view(), name="organization_detail"),
    path("repositorys", RepositoryListView.as_view(), name="repository_list"),
    path("artikelen", ArticleListView.as_view(), name="article_list"),
    path("artikelen/<slug:slug>", ArticleDetailView.as_view(), name="article_detail"),
    path("toevoegen/repository", RepositoryAddView.as_view(), name="repository_add"),
    path("toevoegen/repository/succes", RepositoryAddSuccessView.as_view(), name="repository_add_success"),
    path("statistieken", StatisticsView.as_view(), name="statistics"),
    path("tools/oas-generator", OpenApiSpecGenerator.as_view(), name="oas_generator"),
    path("contact", TemplateView.as_view(title="Contact", template_name="contact.html"), name="contact"),
    path("over", TemplateView.as_view(
        title="Over Developer Overheid",
        description="Eén centrale plek voor de developer die voor of met de overheid ontwikkelt",
        template_name="content/about.html",
    ), name="content_about"),
    path("privacy", TemplateView.as_view(title="Privacyverklaring", template_name="content/privacy.html"),
         name="content_privacy"),
    path(".well-known/security.txt", RedirectView.as_view(url="https://www.ncsc.nl/.well-known/security.txt")),
]


if settings.DEBUG:
    urlpatterns += [
        path("error/400", default_views.bad_request, {"exception": Exception("Bad request")}),
        path("error/404", default_views.page_not_found, {"exception": Exception("Page not Found")}),
        path("error/500", default_views.server_error),
    ]
