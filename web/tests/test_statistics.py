from unittest import TestCase

from influxdb_client.client.flux_table import FluxRecord, FluxTable, TableList

from web.statistics import serialize_design_rules_api_successes


class SerializeTests(TestCase):
    def test_design_rules_api_successes(self):
        t1 = FluxTable()
        t1.records = [
            FluxRecord(table=t1, values={"version": "t1", "successes": 0, "_value": 12}),
            FluxRecord(table=t1, values={"version": "t1", "successes": 1, "_value": 34}),
            FluxRecord(table=t1, values={"version": "t1", "successes": 2, "_value": 56}),
            FluxRecord(table=t1, values={"version": "t1", "successes": 3, "_value": 78}),
        ]

        t2 = FluxTable()
        t2.records = [
            FluxRecord(table=t2, values={"version": "t2", "successes": 0, "_value": 12}),
            FluxRecord(table=t2, values={"version": "t2", "successes": 1, "_value": 42}),
            FluxRecord(table=t2, values={"version": "t2", "successes": 2, "_value": 0}),
        ]

        tables = TableList([t1, t2])
        data = serialize_design_rules_api_successes(tables)

        self.assertEqual(2, len(data))
        self.assertDictEqual(data["t1"], {"0/3": 12, "1/3": 34, "2/3": 56, "3/3": 78})
        self.assertDictEqual(data["t2"], {"0/2": 12, "1/2": 42, "2/2": 0})
