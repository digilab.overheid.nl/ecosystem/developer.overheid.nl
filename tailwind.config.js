/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./web/templates/*.{html,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

