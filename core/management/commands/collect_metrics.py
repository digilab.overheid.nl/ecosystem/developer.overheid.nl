import os

from django.core.management.base import BaseCommand, CommandError

from don import settings
from core.metrics import collect_metrics, new_influxdb_client


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--influxdb-url", default=os.environ.get("INFLUXDB_URL"))
        parser.add_argument(
            "--influxdb-token", default=os.environ.get("INFLUXDB_TOKEN"))

    def handle(self, *args, **options):
        client = new_influxdb_client(options["influxdb_url"], options["influxdb_token"])

        try:
            client.ready()
        except Exception as e:
            raise CommandError(f"Can't connect to InfluxDB: {e}") from e

        with client as c:
            collect_metrics(c, settings.METRICS_INFLUXDB["BUCKET"])
