from datetime import timedelta
import logging
from random import randrange
from typing import Optional

from django.utils import timezone
from django.core.exceptions import ImproperlyConfigured
from django.db.models import Count, Q, F
from django.template.loader import render_to_string
from influxdb_client import InfluxDBClient, QueryApi, Point, WritePrecision
from influxdb_client.client.flux_table import TableList
from influxdb_client.client.write_api import PointSettings
from influxdb_client.rest import ApiException as InfluxDBApiException

from don import settings
from core.models.api import API, Organization, APIValidatorReport
from core.models.repository import Repository
from core.models.validator import ValidatorRuleset


WRITE_PRECISION = WritePrecision.S

logger = logging.getLogger(__name__)


def collect_metrics(influxdb_client: InfluxDBClient, bucket: str):
    points = (
        _organization_point(),
        _api_point(),
        _repository_point(),
        _api_design_rules_per_rule(),
        _api_design_rules_passed_grouped(),
    )

    with influxdb_client.write_api() as api:
        api.write(bucket=bucket, record=points, write_precision=WRITE_PRECISION)


def _organization_point() -> Point:
    return Point("organization").field("total_count", Organization.objects.count())


def _api_point() -> Point:
    count = API.objects.count()

    type_counts = {
        t["api_type"]: t["count"] for t in
        API.objects.values("api_type").annotate(count=Count("api_type")).order_by()}

    auth_counts = {
        t["api_authentication"]: t["count"] for t in
        (API.objects
            .values("api_authentication")
            .annotate(count=Count("api_authentication"))
            .order_by())
    }

    point = Point("api").field("total_count", count)

    for x in API.APIType:
        point.field(f"type_{x}_count", type_counts.get(x, 0))
    for x in API.APIAuthentication:
        point.field(f"auth_{x}_count", auth_counts.get(x, 0))

    return point


def _repository_point() -> Point:
    count = Repository.objects.count()

    source_counts = {
        t["source"]: t["count"] for t in
        Repository.objects.values("source").annotate(count=Count("source")).order_by()}

    point = Point("repository") \
        .field("total_count", count) \
        .field("source_github_count", source_counts.get(Repository.Source.GITHUB, 0)) \
        .field("source_gitlab_count", source_counts.get(Repository.Source.GITLAB, 0))

    return point


def _api_design_rules_per_rule() -> list[Point]:
    ruleset_versions = ValidatorRuleset.objects \
        .prefetch_related("rules") \
        .filter(ruleset_id="core")

    stats = {}
    for ruleset in ruleset_versions:
        bucket = APIValidatorReport.objects \
            .values(rule_id=F("report__results__rule__rule_id")) \
            .annotate(
                passed_count=Count("rule_id", filter=Q(report__results__passed=True)),
                total_count=Count("rule_id")) \
            .filter(
                is_latest=True,
                report__ruleset_id=ruleset.id,
            )
        stats[ruleset.version] = bucket

    return [
        (
            Point("design_rule")
            .tag("version", version)
            .tag("rule", result["rule_id"])
            .field("total_count", result["total_count"])
            .field("success_count", result["passed_count"])
        )
        for version, bucket in stats.items()
        for result in bucket
    ]


def _api_design_rules_passed_grouped() -> list[Point]:
    ruleset_versions = ValidatorRuleset.objects \
        .values("id", "version", rules_count=Count("rules")) \
        .filter(ruleset_id="core")

    stats = {}
    for ruleset in ruleset_versions:
        buckets = dict.fromkeys(range(ruleset["rules_count"] + 1), 0)
        counts = APIValidatorReport.objects \
            .values(passed_count=F("report__rule_passed_count")) \
            .annotate(api_count=Count("passed_count")) \
            .filter(
                is_latest=True,
                report__ruleset_id=ruleset["id"],
            )
        for count in counts:
            buckets[count["passed_count"]] = count["api_count"]

        stats[ruleset["version"]] = buckets

    return [
        (Point("design_rule_api_success")
            .tag("version", version)
            .tag("successes", passed_count)
            .field("apis", api_count)
         )
        for version, buckets in stats.items()
        for passed_count, api_count in buckets.items()
    ]


def execute_query(name: str, api: QueryApi, start_days: int, window_every: int) -> TableList:
    query = render_to_string(f"influx/{name}.flux", {
        "bucket": settings.METRICS_INFLUXDB["BUCKET"],
    })
    params = {
        "start": timedelta(days=start_days),
        "stop": timezone.now(),
        "window_every": window_every,
    }

    try:
        result = api.query(query, params=params)
    except InfluxDBApiException as e:
        logger.error("Error executing query: %s", e)
        raise

    return result


def new_influxdb_client(url: Optional[str] = None, token: Optional[str] = None) -> InfluxDBClient:
    if url is None:
        url = settings.METRICS_INFLUXDB["URL"]
    if token is None:
        token = settings.METRICS_INFLUXDB["TOKEN"]

    client = InfluxDBClient(url=url, token=token, org=settings.METRICS_INFLUXDB["ORGANIZATION"])

    try:
        client.ready()
    except Exception as e:
        raise ImproperlyConfigured(f"Can't connect to InfluxDB: {e}") from e

    return client


def generate_dummy_metrics(influxdb_client: InfluxDBClient, bucket: str):
    points = []

    base = timezone.now().replace(hour=14, minute=42, second=12, microsecond=0)
    for d in range(365):
        day = base - timedelta(days=d)

        api_total_count = randrange(50, 200)

        points.extend((
            Point("organization").field("total_count", randrange(10, 50)).time(day),
            Point("api").field("total_count", api_total_count).time(day),
            Point("repository").field("total_count", randrange(100, 800)).time(day),
        ))

        points.extend(
            (Point("design_rule")
                .tag("version", "dummy")
                .tag("rule", rule)
                .field("total_count", api_total_count)
                .field("success_count", randrange(0, api_total_count))
                .time(day))
            for rule in ("DUMMY-01", "DUMMY-02", "DUMMY-03", "DUMMY-04", "DUMMY-05")
        )

        points.extend(
            (Point("design_rule_api_success")
                .tag("version", "dummy")
                .tag("successes", success_count)
                .field("apis", randrange(0, api_total_count))
                .time(day))
            for success_count in range(6)
        )

    with influxdb_client.write_api(point_settings=PointSettings(dummy="yes")) as api:
        api.write(bucket=bucket, record=points, write_precision=WRITE_PRECISION)
