from .api import APIMetadataValidator, ADRValidator
from .repository import RepositoryValidator
from .base import ValidatorRegistry


_settings = {
    "api-metadata": APIMetadataValidator,
    "adr": ADRValidator,
    "repository": RepositoryValidator,
}

validators = ValidatorRegistry(_settings)
