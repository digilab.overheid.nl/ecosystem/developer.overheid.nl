from collections import OrderedDict

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from core.models.organization import Organization
from core.languages import LANGUAGES
from . import MAX_ENUM_LENGTH, MAX_TEXT_LENGTH, MAX_URL_LENGTH
from .api import API
from .topic import Topic

from .validator import ForeignValidatorReport, BaseValidatorReport, ValidatorReportsMixin


PROGRAMMING_LANGUAGE_USAGE_MIN, PROGRAMMING_LANGUAGE_USAGE_MAX = 0.1, 100.0


class ProgrammingLanguage(models.Model):
    name = models.CharField(max_length=MAX_TEXT_LENGTH, unique=True)

    def color(self):
        return LANGUAGES.get(self.name, "#e0e4ea")

    def __str__(self):
        return self.name


class Repository(ValidatorReportsMixin["RepositoryValidatorReport"], models.Model):
    class Source(models.TextChoices):
        GITLAB = 'gitlab', 'GitLab repository'
        GITHUB = 'github', 'GitHub repository'
        GITLAB_SNIPPET = 'gitlab_snippet', 'GitLab snippet'
        GITHUB_GIST = 'github_gist', 'GitHub gist'

    class Meta:
        verbose_name_plural = 'Repositories'

    organization = models.ForeignKey(Organization, verbose_name=_("organization"), on_delete=models.PROTECT)
    source = models.CharField(
        max_length=MAX_ENUM_LENGTH,
        choices=Source.choices
    )
    owner_name = models.CharField(max_length=MAX_TEXT_LENGTH)
    name = models.CharField(max_length=MAX_TEXT_LENGTH)
    url = models.URLField(max_length=MAX_URL_LENGTH, unique=True)
    description = models.TextField()
    readme_url = models.CharField(max_length=MAX_URL_LENGTH, blank=True)
    last_change = models.DateTimeField(db_index=True)
    stars = models.IntegerField(null=True)
    fork_count = models.IntegerField(default=0)
    issue_open_count = models.IntegerField(null=True)
    merge_request_open_count = models.IntegerField(null=True)
    avatar_url = models.URLField(max_length=MAX_URL_LENGTH, null=True)

    archived = models.BooleanField(default=False)
    last_fetched_at = models.DateTimeField(db_index=True)

    programming_languages = models.ManyToManyField(
        ProgrammingLanguage, through='RepositoryProgrammingLanguage', related_name='repositories')

    related_apis = models.ManyToManyField(
        API, through='RepositoryAPI', related_name='related_repositories')

    topics = models.ManyToManyField(Topic)

    @property
    def main_programming_language(self) -> ProgrammingLanguage:
        r = self.repositoryprogramminglanguage_set.first()
        return r.programming_language if r else None

    def programming_languages_string(self):
        return ', '.join(sorted(p.name for p in self.programming_languages.all()))
    programming_languages_string.short_description = 'programming languages'

    def related_apis_string(self):
        return ', '.join(sorted(a.api_id for a in self.related_apis.all()))
    related_apis_string.short_description = 'related apis'

    def __str__(self):
        return f'{self.owner_name}/{self.name}'


class RepositoryProgrammingLanguage(models.Model):
    repository = models.ForeignKey(Repository, on_delete=models.CASCADE)
    programming_language = models.ForeignKey(ProgrammingLanguage, on_delete=models.CASCADE)
    usage = models.DecimalField(
        max_digits=4,
        decimal_places=1,
        validators=(
            MinValueValidator(PROGRAMMING_LANGUAGE_USAGE_MIN),
            MaxValueValidator(PROGRAMMING_LANGUAGE_USAGE_MAX)))

    class Meta:
        unique_together = (('repository', 'programming_language'),)
        ordering = ('-usage',)


class RepositoryAPI(models.Model):
    repository = models.ForeignKey(Repository, on_delete=models.CASCADE)
    api = models.ForeignKey(API, to_field='api_id', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.api.api_id} <-> {self.repository.owner_name}/{self.repository.name}'


def get_programming_languages_count(
    repository_programming_languages: list[RepositoryProgrammingLanguage]
) -> OrderedDict:
    programming_languages = OrderedDict()
    for repository_language in repository_programming_languages:
        language_set = programming_languages.setdefault(repository_language.programming_language.name, {})
        language_set.setdefault("color", repository_language.programming_language.color())
        usage_count = programming_languages[repository_language.programming_language.name].get("usage_count", 0)

        programming_languages[repository_language.programming_language.name]["usage_count"] = usage_count + 1

    return sorted(programming_languages.items(), key=lambda kv: kv[1]["usage_count"], reverse=True)


class RepositoryValidatorReport(BaseValidatorReport):
    repository = ForeignValidatorReport(Repository)
