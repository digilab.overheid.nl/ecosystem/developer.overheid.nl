from django.db import models

from . import MAX_TEXT_LENGTH


class Topic(models.Model):
    name = models.CharField(max_length=MAX_TEXT_LENGTH, unique=True)

    def __str__(self):
        return self.name
