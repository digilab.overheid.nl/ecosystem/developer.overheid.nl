from django.db import models
from django.utils import timezone

from . import MAX_URL_LENGTH
from .topic import Topic


class ArticleQuerySet(models.QuerySet):
    def published(self):
        return self.filter(published_at__lte=timezone.now())


class Article(models.Model):
    slug = models.CharField(max_length=MAX_URL_LENGTH, unique=True)
    title = models.TextField(max_length=100)
    summary = models.TextField()
    published_at = models.DateField()
    content_html = models.TextField()

    topics = models.ManyToManyField(Topic)

    objects = ArticleQuerySet.as_manager()

    class Meta:
        ordering = ("-published_at",)
