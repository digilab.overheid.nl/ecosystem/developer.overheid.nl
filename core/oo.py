from django.conf import settings
from requests import HTTPError

from core.http import create_http_client


class OrganizationFetchError(RuntimeError):
    pass


class OrganizationNotFoundError(OrganizationFetchError):
    pass


class OrganizationsAPIClient:
    def __init__(self, base_url: str) -> None:
        self._session = create_http_client(base_url)

    def list_organizations(self) -> list[dict]:
        response = self._session.get("/organisaties")
        response.raise_for_status()

        return response.json()

    def get_organization(self, system_id: int) -> dict:
        response = self._session.get(f"/organisaties/{system_id}")
        if response.status_code == 404:
            raise OrganizationNotFoundError()

        try:
            response.raise_for_status()
        except HTTPError as e:
            raise OrganizationFetchError(e) from e

        return response.json()


organizations_api_client = OrganizationsAPIClient(settings.OO_API_URL)
