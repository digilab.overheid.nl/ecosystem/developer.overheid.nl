from subprocess import CompletedProcess, CalledProcessError
from unittest.mock import MagicMock, patch

from django.test import TestCase, SimpleTestCase

from core.tests.models.utils import create_ruleset
from core.models.validator import Validator as ValidatorModel, ValidatorRuleset, ValidatorReport, ValidatorResult
from core.validators.base import (
    InvalidValidatorError, UndefinedRulesetError, ValidatorExecutionError,
    ValidatorRegistry, Validator, CLIValidator)


class RegistryTests(SimpleTestCase):
    class _MockValidator(Validator):
        def load_metadata(self):
            pass

    def test_same_instance(self):
        registry = ValidatorRegistry({"mock": self._MockValidator})
        self.assertIs(registry["mock"], registry["mock"])

    def test_non_existing(self):
        with self.assertRaises(InvalidValidatorError):
            _ = ValidatorRegistry({})["unknown"]

        with self.assertRaises(InvalidValidatorError):
            _ = ValidatorRegistry({"mock": self._MockValidator})["unknown"]

    @patch.object(_MockValidator, "check")
    def test_check(self, mock_check: MagicMock):
        _ = ValidatorRegistry({"mock": self._MockValidator})["mock"]

        mock_check.assert_called_once()


class ValidatorBaseTests(TestCase):
    def test_get_validator_metadata(self):
        with self.assertRaises(NotImplementedError):
            Validator("test-validator").get_validator_metadata()

    def test_get_rulesets_metadata(self):
        with self.assertRaises(NotImplementedError):
            Validator("test-validator").get_rulesets_metadata()

    def test_run(self):
        with self.assertRaises(NotImplementedError):
            Validator("test-validator").run(None, "")

    def test_validate_ruleset_not_defined(self):
        message = "Ruleset 'non-existing' not defined for validator 'test-validator'"
        with self.assertRaisesMessage(UndefinedRulesetError, message):
            Validator("test-validator").validate_ruleset("non-existing", "test-uri")

    @patch.object(Validator, "run")
    def test_validate_ruleset(self, run: MagicMock):
        ruleset = create_ruleset()

        run.return_value = [
            ValidatorResult(passed=True, rule=ruleset.rules.first()),
            ValidatorResult(passed=False, rule=ruleset.rules.last())]


        report = Validator("test-validator").validate_ruleset(ruleset.ruleset_id, "test-uri")

        run.assert_called_once_with(ruleset, "test-uri")

        self.assertEqual(report.uri, "test-uri")
        self.assertEqual(report.ruleset.pk, ruleset.pk)
        self.assertEqual(report.results.count(), 2)


TEST_VALIDATOR_METADATA = Validator.ValidatorMetadata(
    "Test validator", "A validator", "1.2.3", "https://example.com/validator")


@patch.object(Validator, "get_validator_metadata",MagicMock(return_value=TEST_VALIDATOR_METADATA))
@patch.object(Validator, "get_rulesets_metadata")
class ValidatorMetadataTests(TestCase):
    def test_metadata_existing_validator(self, _):
        ValidatorModel.objects.create(pk=1, validator_id="other")
        ValidatorModel.objects.create(pk=2, validator_id="test")

        Validator(validator_id="test").load_metadata()

        self.assertEqual(ValidatorModel.objects.count(), 2)
        self.assertEqual(ValidatorModel.objects.get(pk=2).validator_id, "test")

    def test_metadata_create(self, get_rulesets_metadata: MagicMock):
        get_rulesets_metadata.return_value = [
            (
                Validator.RulesetMetadata(
                    "ruleset-1", "Title 1", "Description 1", "1.0.0", "https://example.com/ruleset-1"),
                [
                    Validator.RuleMetadata("rule-1-a", "Title 1 a", "Description 1 a", "https://example.com/rule-1-a"),
                ],
            ),
            (
                Validator.RulesetMetadata("ruleset-2", "Title 2", "Description 2", "1.0.0"),
                [
                    Validator.RuleMetadata("rule-2-a", "Title a", "Description a"),
                    Validator.RuleMetadata("rule-2-b", "Title b", "Description b"),
                    Validator.RuleMetadata("rule-2-c", "Title c", "Description c"),
                ],
            ),
        ]
        Validator(validator_id="test").load_metadata()

        validator = ValidatorModel.objects.get(validator_id="test")
        self.assertEqual(validator.name, "Test validator")
        self.assertEqual(validator.description, "A validator")
        self.assertEqual(validator.version, "1.2.3")
        self.assertEqual(validator.documentation_url, "https://example.com/validator")
        self.assertEqual(validator.rulesets.count(), 2)

        ruleset_1 = validator.rulesets.get(ruleset_id="ruleset-1")
        self.assertEqual(ruleset_1.title, "Title 1")
        self.assertEqual(ruleset_1.description, "Description 1")
        self.assertEqual(ruleset_1.version, "1.0.0")
        self.assertEqual(ruleset_1.documentation_url, "https://example.com/ruleset-1")
        self.assertEqual(ruleset_1.rules.count(), 1)

        rule = ruleset_1.rules.first()
        self.assertEqual(rule.rule_id, "rule-1-a")
        self.assertEqual(rule.title, "Title 1 a")
        self.assertEqual(rule.description, "Description 1 a")
        self.assertEqual(rule.documentation_url, "https://example.com/rule-1-a")

        ruleset_2 = validator.rulesets.get(ruleset_id="ruleset-2")
        self.assertEqual(ruleset_2.rules.count(), 3)

    def test_metadata_update(self, get_rulesets_metadata: MagicMock):
        validator = ValidatorModel.objects.create(validator_id="test")
        ruleset = validator.rulesets.create(ruleset_id="ruleset", description="Description", version="1.0.0")
        ruleset.rules.create(rule_id="rule", title="Title")

        get_rulesets_metadata.return_value = [
            (
                Validator.RulesetMetadata("ruleset", "Title", "Changed description", "1.0.0"),
                [
                    Validator.RuleMetadata("rule", "Other title", "Other description"),
                ],
            ),
        ]
        Validator(validator_id="test").load_metadata()


        ruleset = validator.rulesets.all()[0]
        self.assertEqual(ruleset.description, "Changed description")

        rule = ruleset.rules.first()
        self.assertEqual(rule.title, "Other title")
        self.assertEqual(rule.description, "Other description")

    def test_metadata_delete_undefined_rulesets(self, get_rulesets_metadata: MagicMock):
        validator = ValidatorModel.objects.create(validator_id="test")
        validator.rulesets.create(ruleset_id="other")
        validator.refresh_from_db()

        get_rulesets_metadata.return_value = [
            (
                Validator.RulesetMetadata("test", "Title", "Description", "1.0.0"),
                [
                    Validator.RuleMetadata("rule-1-a", "Title 1 a", "Description 1 a"),
                ],
            ),
        ]
        Validator(validator_id="test").load_metadata()

        rulesets = validator.rulesets.all()
        self.assertEqual(rulesets.count(), 1)
        self.assertEqual(rulesets[0].ruleset_id, "test")

    def test_metadata_delete_undefined_rules(self, get_rulesets_metadata: MagicMock):
        validator = ValidatorModel.objects.create(validator_id="test")
        ruleset = validator.rulesets.create(ruleset_id="test")
        ruleset.rules.create(rule_id="other")

        get_rulesets_metadata.return_value = [
            (
                Validator.RulesetMetadata("test", "Title", "Description", "1.0.0"),
                [
                   Validator.RuleMetadata("rule-1", "Title 1", "Description 1"),
                ],
            ),
        ]
        Validator(validator_id="test").load_metadata()

        ruleset = validator.rulesets.first()
        rules = ruleset.rules.all()
        self.assertEqual(rules.count(), 1)
        self.assertEqual(rules[0].rule_id, "rule-1")


class TestCLIValidator(CLIValidator):
    name = "Test validator"
    description = "A test validator"
    documentation_url = "https://example.com/"


@patch("subprocess.run")
class CLIValidatorTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.ruleset = create_ruleset()

    def test_load_metadata(self, run: MagicMock):
        stdout_version = """{
            "version": "1.2.3",
            "commit": "",
            "date": "2023-09-25T12:24:42Z"
        }"""
        stdout_rulesets = """[
          {
                "ruleset_id": "test-ruleset-1",
                "title": "Title 1",
                "description": "Description 1",
                "version": "1.0.0",
                "documentation_url": "https://example.com/1",
                "rules": [
                    {
                        "rule_id": "test-rule-a",
                        "title": "Rule A",
                        "description": "Description A",
                        "documentation_url": "https://example.com/1#a"
                    },
                    {
                        "rule_id": "test-rule-b",
                        "title": "Rule B",
                        "description": "Description B",
                        "documentation_url": "https://example.com/1#b"
                    }
                ]
            }
        ]"""

        run.side_effect = (
            CompletedProcess("", 0, stdout=stdout_version, stderr=""),
            CompletedProcess("", 0, stdout=stdout_rulesets, stderr=""),
        )

        validator = TestCLIValidator(validator_id="test", path="/test/path")
        validator.load_metadata()

        model = validator.model
        self.assertEqual(model.version, "1.2.3")
        self.assertEqual(model.documentation_url, "https://example.com/")

        ruleset = ValidatorRuleset.objects.get(ruleset_id="test-ruleset-1")
        self.assertEqual(ruleset.title, "Title 1")
        self.assertEqual(ruleset.description, "Description 1")
        self.assertEqual(ruleset.version, "1.0.0")
        self.assertEqual(ruleset.documentation_url, "https://example.com/1")

        rules = ruleset.rules.all()
        self.assertEqual(len(rules), 2)

        rule = rules.first()
        self.assertEqual(rule.rule_id, "test-rule-a")
        self.assertEqual(rule.title, "Rule A")
        self.assertEqual(rule.description, "Description A")
        self.assertEqual(rule.documentation_url, "https://example.com/1#a")


    def test_run_success(self, run: MagicMock):
        stdout = """[
          {
            "rule_id": "test-rule",
            "passed": true,
            "message": "Test message",
            "details": ""
          },
          {
            "rule_id": "test-rule-2",
            "passed": false,
            "message": "Test message 2",
            "details": "Test details 2"
          }
        ]"""
        run.return_value = CompletedProcess("", 0, stdout=stdout, stderr="")

        results = TestCLIValidator(validator_id="test", path="/test/path").run(self.ruleset, "test-uri")

        # Assert results are savable
        report: ValidatorReport = ValidatorReport.objects.create(ruleset=self.ruleset)
        for result in results:
            result.report = report
            result.save()

        (cmd_arg,), _ = run.call_args
        self.assertEqual(("/test/path", "--format=json", "validate", "test-ruleset", "test-uri"), cmd_arg)

        self.assertEqual(len(results), 2)

        self.assertTrue(results[0].passed)
        self.assertEqual(results[0].message, "Test message")
        self.assertEqual(results[0].details, "")

        self.assertFalse(results[1].passed)
        self.assertEqual(results[1].message, "Test message 2")
        self.assertEqual(results[1].details, "Test details 2")

    def test_run_unexpected_rule(self, run: MagicMock):
        stdout = """[
          {
            "rule_id": "other-rule",
            "passed": true,
            "message": "Test message",
            "details": ""
          }
        ]"""
        run.return_value = CompletedProcess("", 0, stdout=stdout, stderr="")

        with self.assertRaisesMessage(ValidatorExecutionError, "Output contains unexpected rule: other-rule"):
            CLIValidator(validator_id="test", path="/test/path").run(self.ruleset, "test-uri")

    def test_run_failure_process(self, run: MagicMock):
        run.side_effect = CalledProcessError(returncode=1, cmd="cmd", output="an error")

        with self.assertRaisesMessage(ValidatorExecutionError, "Process failed: an error"):
            CLIValidator(validator_id="test", path="/test/path").run(self.ruleset, "test-uri")

    def test_run_failure_output(self, run: MagicMock):
        run.return_value = CompletedProcess("", 0, stdout='invalid: json', stderr="")

        message = "Decoding JSON output: Expecting value: line 1 column 1 (char 0)"
        with self.assertRaisesMessage(ValidatorExecutionError, message):
            CLIValidator(validator_id="test", path="/test/path").run(self.ruleset, "test-uri")
