from unittest import mock

from django.test import TestCase

from core.oo import OrganizationNotFoundError
from core.models.organization import Organization


class OrganizationFetchTests(TestCase):
    def test_save_slug(self):
        org = Organization.objects.create(ooid=1, name="Test organization")
        self.assertEqual(org.slug, "test-organization")

        org = Organization(ooid=2, name="Other organization", slug="other")
        self.assertEqual(org.slug, "other")


    @mock.patch("core.oo.organizations_api_client.get_organization")
    def test_get_or_fetch(self, mock_get: mock.Mock):
        mock_get.return_value = {
            "systeem_id": 42,
            "naam": "Test Organization",
            "types": [
                "abcd 1234",
                "other",
            ],
            "beschrijving": "This is a description",
            "adressen": None,
            "contact": {
                "telefoonnummers": None,
                "emailadressen": None,
                "internetadressen": [
                    {
                        "url": "https://example.com",
                        "label": "algemeen",
                    }
                ],
            },
        }

        org: Organization
        org, created = Organization.objects.get_or_fetch(ooid=42)
        self.assertTrue(created)

        self.assertEqual(org.ooid, 42)
        self.assertEqual(org.name, "Test Organization")
        self.assertEqual(org.slug, "test-organization")
        self.assertEqual(org.type, "abcd 1234")
        self.assertEqual(org.description, "This is a description")
        self.assertEqual(
            org.contact,
            {
                "telefoonnummers": None,
                "emailadressen": None,
                "internetadressen": [
                    {
                        "url": "https://example.com",
                        "label": "algemeen",
                    }
                ],
            })

        mock_get.assert_called_once_with(42)

    @mock.patch("core.oo.organizations_api_client.get_organization")
    def test_get_fetch_non_existing_organization(self, mock_get: mock.Mock):
        mock_get.side_effect = OrganizationNotFoundError()

        with self.assertRaises(Organization.DoesNotExist):
            Organization.objects.get_or_fetch(ooid=42)

    @mock.patch("core.oo.organizations_api_client.get_organization")
    def test_update_fetch_non_existing_organization(self, mock_get: mock.Mock):
        mock_get.side_effect = OrganizationNotFoundError()

        Organization.objects.create(ooid=42)

        with self.assertRaises(OrganizationNotFoundError):
            Organization.objects.fetch_and_update(ooid=42)

    @mock.patch("core.oo.organizations_api_client.get_organization")
    def test_fetch_and_update(self, mock_get: mock.Mock):
        mock_get.return_value = {
            "systeem_id": 42,
            "naam": "Other organization",
            "types": None,
            "beschrijving": None,
            "adressen": None,
            "contact": None,
        }

        Organization.objects.create(
            ooid=42,
            name="Organization name",
            type="A type",
            description="A description",
            contact={"key": "value"},
        )

        Organization.objects.fetch_and_update(ooid=42)
        org: Organization = Organization.objects.get(ooid=42)

        self.assertEqual(org.name, "Other organization")
        self.assertEqual(org.slug, "other-organization")
        self.assertIsNone(org.type)
        self.assertIsNone(org.description)
        self.assertIsNone(org.contact)


class OrganizationContactTests(TestCase):
    def test_with_values(self):
        Organization.objects.create(
            ooid=1,
            name="Test organization",
            contact={
                "telefoonnummers": [
                  {
                     "nummer": "012 345 67 89",
                     "label": "algemeen",
                  },
                  {
                     "nummer": "42 42 42",
                     "label": "algemeen",
                  },
                ],
                "emailadressen": [
                  {
                    "email": "info@example.com",
                    "label": "algemeen",
                   }
                ],
                "internetadressen": [
                  {
                    "url": "https://example.com",
                    "label": "algemeen"
                  }
                ],
            })
        org: Organization = Organization.objects.get(ooid=1)

        self.assertEqual(len(org.contact_phone_numbers), 2)
        self.assertEqual(org.contact_phone_numbers[0], "012 345 67 89")
        self.assertEqual(org.contact_phone_numbers[1], "42 42 42")

        self.assertEqual(len(org.contact_email_addresses), 1)
        self.assertEqual(org.contact_email_addresses[0], "info@example.com")

        self.assertEqual(len(org.contact_website_urls), 1)
        self.assertEqual(org.contact_website_urls[0], "https://example.com")

    def test_empty_fields(self):
        Organization.objects.create(
            ooid=1,
            name="Test organization",
            contact={"telefoonnummers": [], "emailadressen": None})
        org: Organization = Organization.objects.get(ooid=1)

        self.assertIsNone(org.contact_phone_numbers)
        self.assertIsNone(org.contact_email_addresses)
        self.assertIsNone(org.contact_website_urls)

    def test_no_contact(self):
        Organization.objects.create(ooid=1, name="Test organization", contact=None)
        org: Organization = Organization.objects.get(ooid=1)

        self.assertIsNone(org.contact_phone_numbers)
        self.assertIsNone(org.contact_email_addresses)
        self.assertIsNone(org.contact_website_urls)
