from datetime import datetime, timezone
from django.test import TestCase

from core.models.organization import Organization
from core.models.repository import Repository, ProgrammingLanguage, RepositoryProgrammingLanguage


class RepositoryTests(TestCase):
    def test_main_programming_language(self):
        repository_a = Repository.objects.create(
            organization=Organization.objects.create(name="Test organization A", ooid=1),
            source=Repository.Source.GITLAB,
            url="a",
            owner_name="test-owner-a",
            name="test-repository-a",
            last_change=datetime(2023, 7, 11, 12, 34, 56, tzinfo=timezone.utc),
            last_fetched_at=datetime(2023, 7, 13, 12, 34, 56, tzinfo=timezone.utc),
        )

        repository_b = Repository.objects.create(
            organization=Organization.objects.create(name="Test organization B", ooid=2),
            source=Repository.Source.GITLAB,
            url="b",
            owner_name="test-owner-b",
            name="test-repository-b",
            last_change=datetime(2023, 7, 11, 12, 34, 56, tzinfo=timezone.utc),
            last_fetched_at=datetime(2023, 7, 13, 12, 34, 56, tzinfo=timezone.utc),
        )

        RepositoryProgrammingLanguage.objects.create(
            repository=repository_b,
            programming_language=ProgrammingLanguage.objects.create(name="JavaScript"),
            usage=42.2,
        )

        RepositoryProgrammingLanguage.objects.create(
            repository=repository_b,
            programming_language=ProgrammingLanguage.objects.create(name="Python"),
            usage=57.8,
        )


        self.assertEqual(repository_a.main_programming_language, None)
        self.assertEqual(repository_b.main_programming_language.name, "Python")
