from unittest.mock import MagicMock, patch
from datetime import datetime, date

from django.test import TestCase

from core.models.article import Article


@patch('django.utils.timezone.now')
class ArticleTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        Article.objects.create(slug="2000", published_at=date(2000, 1, 2))

    def test_published_past(self, now: MagicMock):
        now.return_value = datetime(2000, 1, 3, 16, 37, 00)
        self.assertEqual(Article.objects.published().count(), 1)

    def test_published_same_day(self, now: MagicMock):
        now.return_value = datetime(2000, 1, 2, 16, 37, 00)
        self.assertEqual(Article.objects.published().count(), 1)

    def test_published_future(self, now: MagicMock):
        now.return_value = datetime(2000, 1, 1, 16, 37, 00)
        self.assertEqual(Article.objects.published().count(), 0)
