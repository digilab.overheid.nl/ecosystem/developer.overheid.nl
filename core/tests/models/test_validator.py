from django.test import TestCase

from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.db import connection, models
from django.db.utils import IntegrityError

from core.models.validator import ValidatorReport, BaseValidatorReport, ValidatorResult

from .utils import create_ruleset


class ValidatorReportTests(TestCase):
    def test_create_without_results(self):
        ruleset = create_ruleset()
        with self.assertRaisesMessage(ValidationError, "Can't save report without results"):
            ValidatorReport.objects.create_with_results(ruleset=ruleset, results=[])

    def test_create_with_results(self):
        ruleset = create_ruleset()
        result = ValidatorResult(passed=True, rule=ruleset.rules.first())
        ValidatorReport.objects.create_with_results(ruleset=ruleset, results=[result])

        report = ValidatorReport.objects.first()
        self.assertEqual(report.rule_total_count, 1)
        self.assertEqual(report.rule_passed_count, 1)

        result = ValidatorResult.objects.first()
        self.assertIsNotNone(result.pk)


class BaseValidatorReportTests(TestCase):
    @classmethod
    def setUpClass(cls):
        class TestModel(models.Model):
            field = models.CharField()

        class TestReport(BaseValidatorReport):
            test = models.ForeignKey(TestModel, on_delete=models.CASCADE)

        cls._TestModel = TestModel
        cls._ReportModel = TestReport

        with connection.schema_editor() as schema_editor:
            schema_editor.create_model(cls._TestModel)
            schema_editor.create_model(cls._ReportModel)

        super().setUpClass()

    @classmethod
    def setUpTestData(cls):
        cls.test_obj = cls._TestModel.objects.create()
        cls.ruleset = create_ruleset()

    @classmethod
    def tearDownClass(cls):
        with connection.schema_editor() as schema_editor:
            schema_editor.delete_model(cls._ReportModel)
            schema_editor.delete_model(cls._TestModel)

        super().tearDownClass()

    def _create_report(self, ruleset=None):
        return ValidatorReport.objects.create(ruleset=ruleset or self.ruleset)

    def test_missing_foreign_key_field(self):
        with self.assertRaisesMessage(ImproperlyConfigured, "missing ForeignKey field"):
            class _EmptyReport(BaseValidatorReport):
                other = models.CharField()

    def test_existing_report(self):
        report = self._create_report()
        self._ReportModel.objects.create(test=self.test_obj, report=report)

        with self.assertRaises(IntegrityError):
            self._ReportModel.objects.create(test=self.test_obj, report=report)

    def test_update_is_latest_same_ruleset(self):
        report_1 = self._ReportModel.objects.create(test=self.test_obj, report=self._create_report())
        self.assertTrue(report_1.is_latest)

        report_2 = self._ReportModel.objects.create(test=self.test_obj, report=self._create_report())
        report_1.refresh_from_db()

        self.assertFalse(report_1.is_latest, False)
        self.assertTrue(report_2.is_latest, True)

    def test_update_is_latest_other_ruleset(self):
        ruleset_a = create_ruleset()
        report_a_1 = self._ReportModel.objects.create(test=self.test_obj, report=self._create_report(ruleset_a))

        ruleset_b = create_ruleset()
        report_b_1 = self._ReportModel.objects.create(test=self.test_obj, report=self._create_report(ruleset_b))

        self.assertTrue(report_a_1.is_latest)
        self.assertTrue(report_b_1.is_latest)

        report_a_2 = self._ReportModel.objects.create(test=self.test_obj, report=self._create_report(ruleset_a))
        report_b_2 = self._ReportModel.objects.create(test=self.test_obj, report=self._create_report(ruleset_b))

        self.assertTrue(report_a_2.is_latest)
        self.assertTrue(report_b_2.is_latest)

        report_a_1.refresh_from_db()
        report_b_1.refresh_from_db()

        self.assertFalse(report_a_1.is_latest)
        self.assertFalse(report_b_1.is_latest)
