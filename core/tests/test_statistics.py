from django.test import TestCase

from core.metrics import _api_design_rules_per_rule, _api_design_rules_passed_grouped
from core.models.api import API, APIValidatorReport
from core.models.organization import Organization
from core.models.validator import ValidatorRule, ValidatorReport, ValidatorRuleset, ValidatorResult, Validator


class MetricGenerationTests(TestCase):
    # pylint: disable=protected-access
    @classmethod
    def setUpTestData(cls):
        cls.organization = Organization.objects.create(ooid=1)
        validator = Validator.objects.create()
        cls.ruleset = ValidatorRuleset.objects.create(ruleset_id="core", version="1.0.0", validator=validator)
        ValidatorRule.objects.create(rule_id="1", ruleset=cls.ruleset)
        ValidatorRule.objects.create(rule_id="2", ruleset=cls.ruleset)
        ValidatorRule.objects.create(rule_id="3", ruleset=cls.ruleset)

    def test_apis_per_rule(self):
        rules = self.ruleset.rules.all()

        api1 = API.objects.create(api_id="1", organization=self.organization)
        api2 = API.objects.create(api_id="2", organization=self.organization)

        results1 = [
            ValidatorResult(passed=True, rule=rules[0]),
            ValidatorResult(passed=True, rule=rules[1]),
            ValidatorResult(passed=False, rule=rules[2]),
        ]
        results2 = [
            ValidatorResult(passed=True, rule=rules[0]),
            ValidatorResult(passed=False, rule=rules[1]),
            ValidatorResult(passed=False, rule=rules[2]),
        ]

        report1 = ValidatorReport.objects.create_with_results(results=results1, ruleset=self.ruleset)
        report2 = ValidatorReport.objects.create_with_results(results=results2, ruleset=self.ruleset)

        APIValidatorReport.objects.create(api=api1, report=report1)
        APIValidatorReport.objects.create(api=api2, report=report2)

        points = _api_design_rules_per_rule()

        self.assertEqual(len(points), len(rules))

        self.assertEqual(points[0]._fields["success_count"], 2)
        self.assertEqual(points[0]._fields["total_count"], 2)

        self.assertEqual(points[1]._fields["success_count"], 1)
        self.assertEqual(points[1]._fields["total_count"], 2)

        self.assertEqual(points[2]._fields["success_count"], 0)
        self.assertEqual(points[2]._fields["total_count"], 2)

    def test_apis_per_score(self):
        rules = self.ruleset.rules.all()

        api1 = API.objects.create(api_id="1", organization=self.organization)
        api2 = API.objects.create(api_id="2", organization=self.organization)

        results1 = [
            ValidatorResult(passed=False, rule=rules[0]),
            ValidatorResult(passed=False, rule=rules[1]),
            ValidatorResult(passed=False, rule=rules[2]),
        ]
        results2 = [
            ValidatorResult(passed=True, rule=rules[0]),
            ValidatorResult(passed=True, rule=rules[1]),
            ValidatorResult(passed=False, rule=rules[2]),
        ]

        report1 = ValidatorReport.objects.create_with_results(results=results1, ruleset=self.ruleset)
        report2 = ValidatorReport.objects.create_with_results(results=results2, ruleset=self.ruleset)

        APIValidatorReport.objects.create(api=api1, report=report1)
        APIValidatorReport.objects.create(api=api2, report=report2)

        points = _api_design_rules_passed_grouped()

        self.assertEqual(len(points), len(rules) + 1)

        self.assertEqual(points[0]._tags["successes"], 0)
        self.assertEqual(points[0]._fields["apis"], 1)

        self.assertEqual(points[1]._tags["successes"], 1)
        self.assertEqual(points[1]._fields["apis"], 0)

        self.assertEqual(points[2]._tags["successes"], 2)
        self.assertEqual(points[2]._fields["apis"], 1)

        self.assertEqual(points[3]._tags["successes"], 3)
        self.assertEqual(points[3]._fields["apis"], 0)
