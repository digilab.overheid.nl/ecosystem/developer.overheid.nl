const { terminalLog, sizes, config } = require("../support/e2e");

describe('API Detail', () => {
  beforeEach(() => {
    cy.visit('/apis/kadaster-bag-current')
  })

  it('navigating to the first API Detail page', () => {
    cy.get('a').contains("Specificatie")
  })

  it('has links to forum posts when available', () => {
    cy.visit('/apis/kadaster-brt')
    cy.get('.forum h2').contains('Forum')
  })

  it(`renders OAS specs`, () => {
    cy.contains('Specificatie').click()
    cy.get('#specification-wrapper .menu-content', {timeout: 10000})

    cy.contains('Huidige bevragingen API').toMatchImageSnapshot();
    cy.contains('Huidige bevragingen API').screenshot()
  })

  context('a11y', () => {
    sizes.forEach(size => {
      it(`${size.toString().replace(",", "x")}: has no detectable a11y violations on load`, () => {
        if (Cypress._.isArray(size)) {
          cy.viewport(size[0], size[1])
        } else {
          cy.viewport(size)
        }
        cy.visit('/apis/kadaster-bag-current')

        cy.injectAxe()
        // Test the page at initial load
        cy.checkA11y(null, config, terminalLog)
      })
    })
  })

})
