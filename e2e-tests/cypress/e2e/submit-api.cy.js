const { terminalLog, sizes, config } = require("../support/e2e")

describe('Submit API', () => {
  it('should validate the required fields', () => {
    cy.visit('/apis/add')

    cy.get('#field_production_api_url').type('http://example.com')
    cy.get('.add-api-form').submit()

    cy.get('#field_service_name').siblings('.field-wrapper__errors').should('exist')
    cy.get('#field_description').siblings('.field-wrapper__errors').should('exist')
    cy.get('#field_production_api_url').siblings('.field-wrapper__errors').should('exist')
  })

  context('a11y', () => {
    sizes.forEach(size => {
      it(`${size.toString().replace(",", "x")}: has no detectable a11y violations on load`, () => {
        if (Cypress._.isArray(size)) {
          cy.viewport(size[0], size[1])
        } else {
          cy.viewport(size)
        }
        cy.visit('/apis/add')
        cy.screenshot().toMatchImageSnapshot();

        cy.injectAxe()
        // Test the page at initial load
        cy.checkA11y(null, config, terminalLog)
      })
    })
  })

})
