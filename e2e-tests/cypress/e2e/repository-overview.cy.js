const { terminalLog, sizes, config } = require("../support/e2e")

describe('Repository Overview', () => {
  const baseUrl = '/repositories'

  it('should have a text search field', () => {
    cy.visit(baseUrl)
    cy.get('#query').type("this should not be found")
    cy.get('form.facet-filters').submit()
    cy.contains("Geen repository's gevonden.")
  })

  it('should have filters for technology', () => {
    cy.visit(baseUrl)
    cy.intercept('GET', `${baseUrl}?*`).as('filter')

    const option = cy.get('.facet-filters__list__heading')
      .contains('Technologie')
      .parent().find('.facet-filters__list__option').last();

    option.find('input').click()

    option.parent().then((elem) => {
      const count = /\((\d+)\)/.exec(elem.text())[1]
      cy.get('form.facet-filters').submit()
      cy.wait('@filter')
      cy.get('.results-count').contains(`${count} repository`)
    })
  })

  it('resets the page when filtering', () => {
    cy.visit(baseUrl, {qs: {pagina: 2}})
    cy.get('.facet-filters__list__option input').last().click()
    cy.get('form.facet-filters').submit()
    cy.url().should('not.include', 'pagina=')
  })

  it('displays the page not found when trying to access a non-existing page', () => {
    const options = {url: baseUrl, qs: {pagina: 100}, failOnStatusCode: false}
    cy.visit(options)
    cy.request(options).its('status').should('equal', 404)
  })

  context('a11y', () => {
    sizes.forEach(size => {
      it(`${size.toString().replace(",", "x")}: has no detectable a11y violations on load`, () => {
        if (Cypress._.isArray(size)) {
          cy.viewport(size[0], size[1])
        } else {
          cy.viewport(size)
        }
        cy.visit(baseUrl)
        cy.screenshot().toMatchImageSnapshot();

        cy.injectAxe()
        // Test the page at initial load
        cy.checkA11y(null, config, terminalLog)
      })
    })
  })
})
