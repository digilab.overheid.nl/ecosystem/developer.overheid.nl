from django.urls import include, path, re_path
from rest_framework.routers import DefaultRouter

from . import api_marjor_version
from .views import APIRootView, APIViewSet, OpenAPIView, RepositoryViewSet


class Router(DefaultRouter):
    include_format_suffixes = False
    APIRootView = APIRootView


router = Router(trailing_slash=False)
router.register("/apis", APIViewSet)
router.register("/repositories", RepositoryViewSet)

urlpatterns = [
    path(f"v{api_marjor_version}", include(router.urls)),
    re_path(rf"v{api_marjor_version}/openapi\.(?P<format>json|yaml)", OpenAPIView.as_view(), name="openapi"),
]
