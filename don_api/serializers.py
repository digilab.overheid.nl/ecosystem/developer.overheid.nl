from datetime import UTC

from rest_framework.fields import (
    BooleanField, CharField, DateTimeField, FloatField, IntegerField, URLField, SerializerMethodField)
from rest_framework.serializers import ModelSerializer, Serializer

from core.models.api import API, Environment, Organization
from core.models.repository import Repository


class _NestedOrganizationSerializer(ModelSerializer):
    class Meta:
        model = Organization
        fields = read_only_fields = ("ooid", "name")


class _BaseAPISerializer(ModelSerializer):
    class _EnvironmentsSerializer(ModelSerializer):
        class Meta:
            model = Environment
            fields = read_only_fields = ("name", "api_url", "specification_url", "documentation_url")

    class _ContactSerializer(Serializer):
        email = CharField(source="contact_email")
        phone = CharField(source="contact_phone")
        url = URLField(source="contact_url")

    class _TermsOfUseSerializer(Serializer):
        government_only = BooleanField(source="terms_government_only")
        pay_per_use = BooleanField(source="terms_pay_per_use")
        uptime_guarantee = FloatField(source="terms_uptime_guarantee")
        support_response_time = IntegerField(source="terms_support_response_time")

    id = CharField(source="api_id")
    organization = _NestedOrganizationSerializer()
    environments = _EnvironmentsSerializer(many=True)
    contact = _ContactSerializer(source="*")
    terms_of_use = _TermsOfUseSerializer(source="*")

    class Meta:
        model = API
        fields = read_only_fields = (
            "id",
            "service_name",
            "description",
            "organization",
            "api_type",
            "api_authentication",
            "environments",
            "contact",
            "is_reference_implementation",
            "terms_of_use",
        )


class APIListSerializer(_BaseAPISerializer):
    pass


class APIDetailSerializer(_BaseAPISerializer):
    pass


class RepositoryListSerializer(ModelSerializer):
    organization = _NestedOrganizationSerializer()
    last_change = DateTimeField(default_timezone=UTC)
    programming_languages = SerializerMethodField()

    class Meta:
        model = Repository
        fields = read_only_fields = (
            "source",
            "owner_name",
            "name",
            "organization",
            "description",
            "last_change",
            "url",
            "avatar_url",
            "stars",
            "fork_count",
            "issue_open_count",
            "merge_request_open_count",
            "archived",
            "programming_languages"
        )

    def get_programming_languages(self, obj: Repository):
        return (p.name for p in obj.programming_languages.all())
