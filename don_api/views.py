import json

from django.conf import settings
from django.db.models import F, Prefetch
from django.http import Http404
from django.http.response import HttpResponse
from django.views import View
from rest_framework.mixins import ListModelMixin
from rest_framework.request import Request
from rest_framework.reverse import reverse
from rest_framework.routers import APIRootView as RestAPIRootView
from rest_framework.viewsets import GenericViewSet, ReadOnlyModelViewSet
import yaml

from core.models.api import API
from core.models.repository import Repository, RepositoryProgrammingLanguage

from . import api_version
from .mixins import APIVersionMixin
from .serializers import APIListSerializer, APIDetailSerializer, RepositoryListSerializer


class APIRootView(APIVersionMixin, RestAPIRootView):
    name = "Developer Overheid API"


class APIViewSet(APIVersionMixin, ReadOnlyModelViewSet):
    queryset = API.objects \
        .prefetch_related("environments") \
        .select_related("organization") \
        .order_by("api_id")
    lookup_field = "api_id"
    serializer_class = APIDetailSerializer
    list_serializer_class = APIListSerializer


class RepositoryViewSet(APIVersionMixin, ListModelMixin, GenericViewSet):
    queryset = Repository.objects \
        .prefetch_related(
            Prefetch(
                "repositoryprogramminglanguage_set",
                queryset=RepositoryProgrammingLanguage.objects.order_by("-usage"),
            )
        ) \
        .order_by(F("last_change").desc(nulls_last=True))
    serializer_class = RepositoryListSerializer


class OpenAPIView(APIVersionMixin, View):
    schema_file = settings.BASE_DIR / "don_api" / "schema" / "openapi.yaml"
    json_schema = None
    yaml_schema = None

    def get(self, request: Request, format: str):  # noqa: pylint: redefined-builtin
        if self.yaml_schema is None:
            self.init_schema()

        if format == "json":
            data = self.json_schema
            content_type = "application/json"
        elif format == "yaml":
            data = self.yaml_schema
            content_type = "application/yaml"
        else:
            raise Http404

        headers = None
        if request.headers.get("origin"):
            headers = {"Access-Control-Allow-Origin": "*"}

        return HttpResponse(content=data, content_type=content_type, headers=headers)

    @classmethod
    def init_schema(cls):
        with open(cls.schema_file, "r", encoding="utf-8") as f:
            source_schema = yaml.safe_load(f)

        source_schema["info"]["version"] = api_version
        source_schema["servers"][0]["url"] = reverse("api-root")

        cls.json_schema = json.dumps(source_schema)
        cls.yaml_schema = yaml.dump(source_schema, sort_keys=False)

        del source_schema
