from django.apps import AppConfig


class APIConfig(AppConfig):
    name = "don_api"
