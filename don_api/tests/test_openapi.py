from django.urls import reverse
from rest_framework import status
from rest_framework.test import APISimpleTestCase


class OpenAPITest(APISimpleTestCase):
    def test_json(self):
        url = reverse("openapi", kwargs={"format": "json"})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response["content-type"], "application/json")

    def test_yaml(self):
        url = reverse("openapi", kwargs={"format": "yaml"})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response["content-type"], "application/yaml")
