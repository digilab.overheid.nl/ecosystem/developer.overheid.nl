# static files

Repository with static files supporting open APIs, open repositories, and other functionalities. 

All files in this repository are available at https://developer.overheid.nl/static

**NOTE**: don't create files under the folders `bundles`, `dist`, `images` and
`vendor` because those folders are reserved for the site https://developer.overheid.nl
